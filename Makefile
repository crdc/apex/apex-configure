PROJECT=configure
REGISTRY=registry.gitlab.com/crdc/apex/apex-$(PROJECT)
BUILD_TAG="${CI_COMMIT_TAG:-$(git describe --all | sed -e's/.*\///g')}"
DESTDIR=/usr
CONFDIR=/etc
SYSTEMDDIR=/lib/systemd

M := $(shell printf "\033[34;1m▶\033[0m")

all: build

lint: ; $(info $M Linting the files)
	@./tools/checks lint

test: ; $(info $M Running unittests)
	@./tools/checks test

race: ; $(info $M Running data race detector)
	@./tools/checks race

msan: ; $(info $M Running memory sanitizer)
	@./tools/checks msan

coverage: ; $(info $M Generating global code coverage report)
	@./tools/coverage

coverhtml: ; $(info $M Generating global code coverage report in HTML)
	@./tools/coverage html

build: ; $(info $(M) Building project...)
	@./tools/build

static: ; $(info $(M) Building static executable...)
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build \
	-a -tags netgo -ldflags '-w -extldflags "-static"' \
	-o bin/plantd-configure-static cmd/configure/*.go

image: ; $(info $(M) Building application image...)
	@docker build -t $(REGISTRY) -f ./build/Dockerfile .

container: image ; $(info $(M) Running application container...)
	@docker run -p 4000:4000 $(REGISTRY):latest

# FIXME: this assumes master branch and os/arch, consider using build output
install: ; $(info $(M) Installing plantd $(PROJECT) service...)
	@install -Dm 755 bin/$(PROJECT)-$(BUILD_TAG)-linux-amd64 "$(DESTDIR)/bin/plantd-$(PROJECT)"
	@install -Dm 644 README.md "$(DESTDIR)/share/doc/plantd/$(PROJECT)/README"
	@install -Dm 644 LICENSE "$(DESTDIR)/share/licenses/plantd/$(PROJECT)/COPYING"
	@mkdir -p "$(CONFDIR)/plantd"
	@mkdir -p /run/plantd
	@install -Dm 644 configs/$(PROJECT).yaml "$(CONFDIR)/plantd/$(PROJECT).yaml"
	@install -Dm 644 init/plantd-$(PROJECT).service "$(SYSTEMDDIR)/system/plantd-$(PROJECT).service"

uninstall: ; $(info $(M) Uninstalling plantd $(PROJECT) service...)
	@rm $(DESTDIR)/bin/plantd-$(PROJECT)

clean: ; $(info $(M) Removing generated files...)
	@rm -rf bin/

.PHONY: all build container coverage coverhtml dep image lint test race msan install uninstall clean
