package model

import (
	pb "gitlab.com/crdc/apex/go-apex/proto/v1"
	//"github.com/golang/protobuf/proto"
	//"github.com/golang/protobuf/ptypes/any"
)

// TODO: make this work with the any type
type Property struct {
	Key   string `bson:"key"   json:"key"`
	Value string `bson:"value" json:"value"`
	//Value any.Any `bson:"value" json:"value"`
}

func (p Property) ToProtobuf() (*pb.Property, error) {
	//data, err := proto.Marshal(p.Value)
	//if err != nil {
	//return nil, err
	//}

	return &pb.Property{
		Key:   p.Key,
		Value: p.Value,
		//Value: &any.Any{
		//TypeUrl: proto.MessageName(p.Value),
		//Value:   data,
		//},
	}, nil
}

func (p Property) FromProtobuf(property pb.Property) error {

	return nil
}

// ConvertProperties is a utility function that converts a list of protobuf properties to the Property model
func ConvertProperties(input []*pb.Property) []*Property {
	if len(input) < 1 {
		return nil
	}
	var properties []*Property
	for _, property := range input {
		properties = append(properties, &Property{
			Key:   property.Key,
			Value: property.Value,
		})
	}
	return properties
}
