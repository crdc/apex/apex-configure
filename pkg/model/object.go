package model

import (
	"log"

	pb "gitlab.com/crdc/apex/go-apex/proto/v1"

	//"github.com/mongodb/mongo-go-driver/bson/objectid"
	"github.com/mongodb/mongo-go-driver/bson/primitive"
)

type Object struct {
	ID         primitive.ObjectID `bson:"_id,omitempty" json:"_id,omitempty"`
	Name       string             `bson:"name"          json:"name"`
	Type       string             `bson:"type"          json:"type"`
	Objects    []*Object          `bson:"objects" 	    json:"objects"`
	Properties []*Property        `bson:"properties"    json:"properties"`
}

func (o Object) ToProtobuf() (*pb.Object, error) {
	var objects []*pb.Object
	for _, obj := range o.Objects {
		_obj, err := obj.ToProtobuf()
		if err != nil {
			return nil, err
		}
		objects = append(objects, _obj)
	}

	var properties []*pb.Property
	for _, prop := range o.Properties {
		_prop, err := prop.ToProtobuf()
		if err != nil {
			return nil, err
		}
		properties = append(properties, _prop)
	}

	return &pb.Object{
		Id:         o.ID.Hex(),
		Name:       o.Name,
		Type:       o.Type,
		Objects:    objects,
		Properties: properties,
	}, nil
}

func (o Object) FromProtobuf(object pb.Object) error {

	return nil
}

// ConvertObjects is a utility function that converts a list of protobuf objects to the Object model
func ConvertObjects(input []*pb.Object) ([]*Object, error) {
	if len(input) < 1 {
		return nil, nil
	}
	var objects []*Object
	for _, object := range input {
		writeObject := Object{}
		// Handle ID
		if object.GetId() != "" {
			objectID, idErr := primitive.ObjectIDFromHex(object.Id)
			if idErr != nil {
				log.Printf("Unable to convert %s to ObjectID", object.Id)
				return nil, idErr
			}
			writeObject.ID = objectID
		} else {
			writeObject.ID = primitive.NewObjectID()
		}
		// Handle Name
		if object.GetName() != "" {
			writeObject.Name = object.GetName()
		}
		// Handle Objects
		if object.GetObjects() != nil {
			convertedObjects, objErr := ConvertObjects(object.GetObjects())
			if objErr != nil {
				// log something
				log.Printf("Unable to convert object")
				return nil, objErr
			}
			writeObject.Objects = convertedObjects
		}
		// Handle Properties
		if object.GetProperties() != nil {
			writeObject.Properties = ConvertProperties(object.GetProperties())
		}
		objects = append(objects, &writeObject)
	}
	return objects, nil
}
