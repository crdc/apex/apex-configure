package service

import (
	//"encoding/json"

	"errors"
	"log"
	"strings"

	"gitlab.com/crdc/apex/configure/pkg/model"

	pb "gitlab.com/crdc/apex/go-apex/proto/v1"

	"github.com/mongodb/mongo-go-driver/bson"
	//"github.com/mongodb/mongo-go-driver/bson/objectid"
	"github.com/mongodb/mongo-go-driver/bson/primitive"
	"github.com/mongodb/mongo-go-driver/mongo"
	"github.com/mongodb/mongo-go-driver/mongo/options"
	"github.com/op/go-logging"
	"golang.org/x/net/context"
)

type Configure struct {
	db  *mongo.Database
	log *logging.Logger
}

// Create a new configure service.
func NewConfigure(db *mongo.Database, log *logging.Logger) *Configure {
	return &Configure{db: db, log: log}
}

// List the configurations for a given namespace.
func (c *Configure) List(ctx context.Context, in *pb.ConfigurationRequest) (*pb.ConfigurationsResponse, error) {
	c.log.Debug("List called")

	coll := c.db.Collection(strings.ToLower(in.Namespace.String()))

	// TODO: don't need full document, just retrieve ID and namespace
	cur, err := coll.Find(ctx, nil)
	if err != nil {
		return nil, err
	}

	defer cur.Close(ctx)

	var configurations []*pb.Configuration

	for cur.Next(nil) {
		configuration := model.Configuration{}
		err := cur.Decode(&configuration)
		if err != nil {
			c.log.Error("Couldn't decode configuration")
			return nil, err
		}

		// Uncomment to display configuration during testing
		//out, err := json.MarshalIndent(configuration, " ", " ")
		//c.log.Debug("Configuration: ", string(out))

		// Create the protobuf message
		conf := &pb.Configuration{
			Id:         configuration.ID.Hex(),
			Name:       configuration.Name,
			Namespace:  in.Namespace,
			Objects:    []*pb.Object{},
			Properties: []*pb.Property{},
		}

		configurations = append(configurations, conf)
	}

	return &pb.ConfigurationsResponse{
		Configurations: configurations,
	}, nil
}

// List all configurations from every namespace found in the Protobuf files.
func (c *Configure) ListAll(ctx context.Context, in *pb.Empty) (*pb.ConfigurationsResponse, error) {
	var configurations []*pb.Configuration

	c.log.Debug("ListAll called")

	for key, value := range pb.Configuration_Namespace_name {
		coll := c.db.Collection(strings.ToLower(value))

		// TODO: don't need full document, just retrieve ID and namespace
		cur, err := coll.Find(ctx, nil)
		if err != nil {
			return nil, err
		}

		defer cur.Close(ctx)

		for cur.Next(nil) {
			configuration := model.Configuration{}
			err := cur.Decode(&configuration)
			if err != nil {
				c.log.Error("Couldn't decode configuration")
				return nil, err
			}

			// All that we care about for the list is the ID and namespace
			conf := &pb.Configuration{
				Id:         configuration.ID.Hex(),
				Namespace:  pb.Configuration_Namespace(key),
				Objects:    []*pb.Object{},
				Properties: []*pb.Property{},
			}

			configurations = append(configurations, conf)
		}
	}

	return &pb.ConfigurationsResponse{
		Configurations: configurations,
	}, nil
}

// Create a new configuration in the database using the Protobuf message given.
func (c *Configure) Create(ctx context.Context, in *pb.ConfigurationRequest) (*pb.ConfigurationResponse, error) {
	log.Printf("Create called for %s", in.Namespace)

	// Handle a missing ID
	id := primitive.NewObjectID()
	if in.Id != "" {
		c.log.Debug("Object ID exists. Re-using...")
		// Convert to an ObjectID type
		objectID, idErr := primitive.ObjectIDFromHex(in.Id)
		if idErr != nil {
			c.log.Errorf("Unable to convert %s to ObjectID", in.Id)
			return nil, idErr
		}
		id = objectID
	}

	// Handle a missing name
	name := id.Hex()
	if in.GetConfiguration().GetName() != "" {
		name = in.GetConfiguration().GetName()
	}

	// Convert objects to the desired model
	objects, objErr := model.ConvertObjects(in.GetConfiguration().GetObjects())
	if objErr != nil {
		c.log.Errorf("Unable to convert objects.")
		return nil, objErr
	}

	// Create the new document
	conf := model.Configuration{
		ID:         id,
		Name:       name,
		Namespace:  in.GetNamespace().String(),
		Objects:    objects,
		Properties: model.ConvertProperties(in.GetConfiguration().GetProperties()),
	}

	// Insert the document
	coll := c.db.Collection(strings.ToLower(in.Namespace.String()))
	insertResponse, insertErr := coll.InsertOne(ctx, conf)
	if insertErr != nil {
		c.log.Error("Could not insert new configuration")
		return nil, insertErr
	}

	// TODO: Ideally, send the updated protobuf response configuration instead of the old one
	return &pb.ConfigurationResponse{
		Configuration: &pb.Configuration{
			Id:         insertResponse.InsertedID.(primitive.ObjectID).Hex(),
			Name:       name,
			Namespace:  in.Namespace,
			Objects:    in.GetConfiguration().GetObjects(),
			Properties: in.GetConfiguration().GetProperties(),
		},
	}, nil
}

// Read out an entire configuration.
// TODO: create conf.ToProtobuf and do most of this there
func (c *Configure) Read(ctx context.Context, in *pb.ConfigurationRequest) (*pb.ConfigurationResponse, error) {
	coll := c.db.Collection(strings.ToLower(in.Namespace.String()))

	configuration := model.Configuration{}
	id, err := primitive.ObjectIDFromHex(in.Id)
	if err != nil {
		c.log.Error("Invalid ID value provided")
		return nil, err
	}

	err = coll.FindOne(ctx, bson.D{{"_id", id}}).Decode(&configuration)
	if err != nil {
		c.log.Error("Couldn't decode configuration")
		return nil, err
	}

	// Convert the returned configuration into a protobuf configuration
	_configuration, protoErr := configuration.ToProtobuf()
	if protoErr != nil {
		c.log.Error("Error converting configuration into protobuf version")
		return nil, protoErr
	}

	return &pb.ConfigurationResponse{
		Configuration: _configuration,
	}, nil
}

// Update a configuration using the given Protobuf message.
func (c *Configure) Update(ctx context.Context, in *pb.ConfigurationRequest) (*pb.ConfigurationResponse, error) {
	log.Printf("Update called: %s %s", in.Id, in.Namespace.String())
	// TODO: Handle the case where the namespace actually changes the selected DB (ideally it should not)

	// Use the original ID as
	if in.GetConfiguration().GetId() == "" {
		in.GetConfiguration().Id = in.GetId()
	} else if in.GetId() != in.GetConfiguration().GetId() {
		idErr := errors.New("cannot write requested ID as documents are immutable")
		c.log.Error("Provided target ID is not matching the source ID")
		return nil, idErr
	}

	// Convert the source ID to an ObjectID
	sourceID, sourceErr := primitive.ObjectIDFromHex(in.Id)
	if sourceErr != nil {
		c.log.Error("Unable to convert hexID to sourceID")
		return nil, sourceErr
	}

	// Convert the target ID to an ObjectID
	targetID, targetErr := primitive.ObjectIDFromHex(in.GetConfiguration().GetId())
	if targetErr != nil {
		c.log.Error("Unable to convert hexID to targetID")
		return nil, targetErr
	}

	// Convert objects to the desired model
	objects, objErr := model.ConvertObjects(in.GetConfiguration().GetObjects())
	if objErr != nil {
		c.log.Errorf("Unable to convert objects.")
		return nil, objErr
	}

	// Attempt to update the document with UpdateOne
	coll := c.db.Collection(strings.ToLower(in.Namespace.String()))
	filter := bson.D{primitive.E{Key: "_id", Value: sourceID}}

	// Create the new document
	conf := model.Configuration{
		ID:         targetID,
		Name:       in.GetConfiguration().GetName(),
		Namespace:  in.GetNamespace().String(),
		Objects:    objects,
		Properties: model.ConvertProperties(in.GetConfiguration().GetProperties()),
	}

	// Attempt to update the configuration
	configuration := model.Configuration{}
	returnDoc := options.ReturnDocument(options.After)
	replaceOptions := options.FindOneAndReplace()
	replaceOptions.SetReturnDocument(returnDoc)
	updateErr := coll.FindOneAndReplace(ctx, filter, conf, replaceOptions).Decode(&configuration)
	if updateErr != nil {
		c.log.Errorf("Error updating configuration %s", in.Id)
		return nil, updateErr
	}

	// Note: We are currently returning the old configuration
	// Convert the returned configuration into a protobuf configuration
	_configuration, protoErr := configuration.ToProtobuf()
	if protoErr != nil {
		c.log.Error("Error converting configuration into protobuf version")
		return nil, protoErr
	}

	// Return the protobuf configuration as a response
	return &pb.ConfigurationResponse{
		Configuration: _configuration,
	}, nil
}

// Delete a configuration.
func (c *Configure) Delete(ctx context.Context, in *pb.ConfigurationRequest) (*pb.ConfigurationResponse, error) {
	log.Printf("Delete called: %s %s", in.Id, in.Namespace.String())

	// Convert to an ObjectID type
	objectID, idErr := primitive.ObjectIDFromHex(in.Id)
	if idErr != nil {
		c.log.Error("Unable to convert hexID to ObjectID")
		return nil, idErr
	}

	// Attempt to delete the document with DeleteOne
	coll := c.db.Collection(strings.ToLower(in.Namespace.String()))
	filter := bson.D{primitive.E{Key: "_id", Value: objectID}}
	configuration := model.Configuration{}
	delErr := coll.FindOneAndDelete(ctx, filter).Decode(&configuration)
	if delErr != nil {
		c.log.Errorf("Error deleting configuration %s %s", in.Id, in.Namespace.String())
		return nil, delErr
	}

	// Convert the returned configuration into a protobuf configuration
	_configuration, protoErr := configuration.ToProtobuf()
	if protoErr != nil {
		c.log.Error("Error converting configuration into protobuf version")
		return nil, protoErr
	}

	// Return the protobuf configuration as a response
	return &pb.ConfigurationResponse{
		Configuration: _configuration,
	}, nil
}
