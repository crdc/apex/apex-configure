package context

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/mongodb/mongo-go-driver/mongo"
)

func OpenDB(config *Config) (*mongo.Database, error) {
	log.Println("Database is connecting to", config.DBHost, config.DBPort)

	// TODO: add username and password from config
	uri := fmt.Sprintf("mongodb://%s:%s", config.DBHost, config.DBPort)
	client, err := mongo.NewClient(uri)
	if err != nil {
		return nil, fmt.Errorf("todo: couldn't connect to mongo: %v", err)
	}

	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, 20*time.Second)
	defer cancel()

	if err := client.Connect(ctx); err != nil {
		log.Println("Retry database connection in 30 seconds...")
		time.Sleep(time.Duration(30) * time.Second)
		return OpenDB(config)
	}

	db := client.Database(config.DBName)

	log.Println("Database is connected")

	return db, nil
}
