package main

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"regexp"

	gcontext "gitlab.com/crdc/apex/configure/pkg/context"
	"gitlab.com/crdc/apex/configure/pkg/service"

	pb "gitlab.com/crdc/apex/go-apex/proto/v1"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/reflection"
)

func main() {
	if len(os.Args) > 1 {
		r, _ := regexp.Compile("^-v$|^(-{2})?version$")
		if r.Match([]byte(os.Args[1])) {
			fmt.Println(VERSION)
		}
		os.Exit(0)
	}

	config, err := gcontext.LoadConfig(".")
	if err != nil {
		log.Fatalf("failed to read configuration: %s\n", err)
	}

	db, err := gcontext.OpenDB(config)
	if err != nil {
		log.Fatalf("unable to connect to db: %s\n", err)
	}

	ctx := context.Background()
	log := service.NewLogger(config)
	configure := service.NewConfigure(db, log)

	ctx = context.WithValue(ctx, "config", config)
	ctx = context.WithValue(ctx, "log", log)
	ctx = context.WithValue(ctx, "configure", configure)

	// create the channel to listen on
	addr := fmt.Sprintf("%s:%s", config.ServerBind, config.ServerPort)
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		log.Error("failed to listen: %s", err)
	}

	var opts []grpc.ServerOption

	if config.TLSUse {

		log.Info("using TLS credentials")

		// Load the certificates from disk
		certificate, err := tls.LoadX509KeyPair(config.TLSCert, config.TLSKey)
		if err != nil {
			log.Error("could not load server key pair: %s", err)
		}

		// Create a certificate pool from the certificate authority
		certPool := x509.NewCertPool()
		ca, err := ioutil.ReadFile(config.TLSCertAuth)
		if err != nil {
			log.Error("could not read ca certificate: %s", err)
		}

		// Append the client certificates from the CA
		if ok := certPool.AppendCertsFromPEM(ca); !ok {
			log.Error("failed to append client certs")
		}

		// Create the TLS credentials
		creds := credentials.NewTLS(&tls.Config{
			ClientAuth:   tls.RequireAndVerifyClientCert,
			Certificates: []tls.Certificate{certificate},
			ClientCAs:    certPool,
		})

		opts = append(opts, grpc.Creds(creds))
	}

	srv := grpc.NewServer(opts...)
	pb.RegisterConfigureEndpointServer(srv, configure)
	reflection.Register(srv)
	if err := srv.Serve(lis); err != nil {
		log.Error("failed to serve: %v", err)
	}
}
