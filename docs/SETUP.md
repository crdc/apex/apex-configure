# Apex Configure Setup

These are old setup steps. Ideally none of these will be necessary going
forward as Docker images should be used instead. These are "deprecated"
and will be removed in a future release.

## Debian/Ubuntu

```sh
apt-get install mongodb mongodb-server
curl -sL https://deb.nodesource.com/setup_11.x | bash -
apt-get install -y nodejs
```

## Install Dependencies

### Debian/Ubuntu

```sh
apt-get install protobuf-compiler libprotobuf-dev
go get -u github.com/golang/protobuf/protoc-gen-go
```

For me on Debian there was errors about the wrong version of the protobuf
compiler, I fixed this by specifying a version.

```sh
GIT_TAG="v1.2.0"
go get -d -u github.com/golang/protobuf/protoc-gen-go
git -C "$(go env GOPATH)"/src/github.com/golang/protobuf checkout $GIT_TAG
go install github.com/golang/protobuf/protoc-gen-go
```

## Build

Build the gRPC service and messages using the protobuf repo.

```sh
git clone --recurse-submodules https://gitlab.com/crdc/apex/apex-configure
cd configure
make
```

## Using TLS

The `certstrap` utility from Square was used during development to create certificate files.

If a hostname is used:

```sh
certstrap --depot-path cert init --common-name "ca"
certstrap --depot-path cert request-cert --domain example.com
certstrap --depot-path cert sign example.com --CA "ca"
```

If an IP address is used:

```sh
certstrap --depot-path cert init --common-name "ca"
certstrap --depot-path cert request-cert --common-name localhost --ip 127.0.0.1
certstrap --depot-path cert sign localhost --CA "ca"
```

## Testing

The `grpcurl` utility was used to test during development, it can be very handy
to see what the service provides.

Installing `grpcurl`

```sh
go get -u github.com/fullstorydev/grpcurl
go install github.com/fullstorydev/grpcurl/cmd/grpcurl
```

Some examples of using it without TLS:

```sh
grpcurl -plaintext 127.0.0.1:4000 list apex.ConfigureEndpoint
grpcurl -plaintext 127.0.0.1:4000 apex.ConfigureEndpoint.ConfigurationListAll
grpcurl -plaintext 127.0.0.1:4000 describe apex.ConfigureEndpoint.ConfigurationList
grpcurl -plaintext -d @ 127.0.0.1:4000 apex.ConfigureEndpoint.ConfigurationList <<EOM
{
  "id": "test",
  "operation": 0
}
EOM
```

Some examples of using it with TLS:

```sh
grpcurl -cacert cert/ca.crt -cert cert/localhost.crt -key cert/localhost.key \
  127.0.0.1:4000 list apex.ConfigureEndpoint
```

A script with some basic tests using `grpcurl` is also in `tools/test`.
